import setuptools
from distutils.core import setup

setup(
    name='Testsimplecalculator',
    version='0.0.1',
    license="GNU GPLv3"
    author="Killian Massé",
    author_email="killian.masse@cpe.fr",
    description="A small example package",
    long_description=open('README.txt').read(),
    packages=setuptools.find_namespace_packages(where="main"),
    python_requires =">=3.4"
)
